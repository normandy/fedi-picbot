module codeberg.org/normandy/fedi-picbot

go 1.18

require (
	github.com/mattn/go-mastodon v0.0.5
	github.com/stretchr/testify v1.8.0
	gopkg.in/ini.v1 v1.66.6
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
