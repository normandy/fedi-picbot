package main

import (
	"context"
	"errors"
	"fmt"
	"os"

	"codeberg.org/normandy/fedi-picbot/internal/config"
	"codeberg.org/normandy/fedi-picbot/internal/flagparser"
	"codeberg.org/normandy/fedi-picbot/internal/image"
	"github.com/mattn/go-mastodon"
)

const maxRetries = 10

// Registers an instance to a server.
func register() error {
	flags, err := flagparser.ParseRegisterFlags(os.Args[2:])
	if err != nil {
		return err
	}

	app, err := mastodon.RegisterApp(context.Background(), &mastodon.AppConfig{
		Server:     flags.Server,
		ClientName: "fedi-picbot",
		// the go-mastodon library hardcodes these scopes when authenticating, so we have to use the same ones
		Scopes:  "read write follow",
		Website: "https://codeberg.org/normandy/fedi-picbot",
	})
	if err != nil {
		return err
	}

	fmt.Println("Copy these into config.ini:")
	fmt.Printf("Server = %s\n", flags.Server)
	fmt.Printf("ClientID = %s\n", app.ClientID)
	fmt.Printf("ClientSecret = %s\n", app.ClientSecret)
	fmt.Println("Don't forget to fill in your username and password in the config file!")

	return nil
}

// Posts a random image to the configured instance.
func post() error {
	flags, err := flagparser.ParsePostFlags(os.Args[2:])
	if err != nil {
		return err
	}

	config, err := config.ConfigFromIni(flags.ConfigFile)
	if err != nil {
		return err
	}

	client, err := login(config)
	if err != nil {
		return err
	}

	img, err := image.GetImageLoop(flags, maxRetries)
	if err != nil {
		return err
	}
	defer img.Cleanup()

	attachment, err := client.UploadMediaFromReader(context.Background(), img.File)
	if err != nil {
		return err
	}

	status := mastodon.Toot{
		Status:    fmt.Sprintf("Source: %s", img.Source),
		MediaIDs:  []mastodon.ID{attachment.ID},
		Sensitive: img.Sensitive,
	}

	if img.Sensitive {
		status.SpoilerText = "NSFW"
		status.Visibility = "unlisted"
	}

	_, err = client.PostStatus(context.Background(), &status)
	if err != nil {
		return err
	}

	err = img.SaveHash(flags.LastHashPath)
	if err != nil {
		return err
	}

	return nil
}

// Login to a Mastodon server with a given config.
func login(config *config.Config) (*mastodon.Client, error) {
	client := mastodon.NewClient(&config.MastodonConfig)
	err := client.Authenticate(context.Background(), config.Login.Username, config.Login.Password)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, "Error: must use 'post' or 'register' subcommands")
		os.Exit(1)
	}

	var err error = nil
	switch os.Args[1] {
	case "register":
		err = register()
	case "post":
		err = post()
	default:
		err = errors.New("must use 'post' or 'register' subcommands")
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
