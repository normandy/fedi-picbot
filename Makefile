GO ?= go
PREFIX ?= /usr/local
bindir = $(PREFIX)/bin

SOURCES = $(shell find . -type f -name '*.go')

.PHONY: all
all: fedi-picbot

fedi-picbot: $(SOURCES) go.mod go.sum
	$(GO) build -o $@ $(GOFLAGS)

.PHONY: install
install: fedi-picbot
	install -m755 $< $(DESTDIR)$(bindir)/$<

.PHONY: clean
clean:
	rm -f fedi-picbot
