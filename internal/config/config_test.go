package config

import (
	"testing"

	"github.com/mattn/go-mastodon"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestConfigFromIni(t *testing.T) {
	iniContents := []byte(`
Server = https://example.com
ClientID = your-client-id-here
ClientSecret = your-client-secret-here

[Login]
Username = user
Password = password
	`)

	expected := &Config{
		MastodonConfig: mastodon.Config{
			Server:       "https://example.com",
			ClientID:     "your-client-id-here",
			ClientSecret: "your-client-secret-here",
		},
		Login: LoginInfo{
			Username: "user",
			Password: "password",
		},
	}

	actual, err := ConfigFromIni(iniContents)
	require.NoError(t, err, "ConfigFromIni doesn't return an error")

	assert.Equal(t, expected.MastodonConfig.Server, actual.MastodonConfig.Server)
	assert.Equal(t, expected.MastodonConfig.ClientID, actual.MastodonConfig.ClientID)
	assert.Equal(t, expected.MastodonConfig.ClientSecret, actual.MastodonConfig.ClientSecret)
	assert.Equal(t, expected.Login.Username, actual.Login.Username)
	assert.Equal(t, expected.Login.Password, actual.Login.Password)
}
