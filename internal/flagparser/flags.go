package flagparser

import (
	"errors"
	"flag"
	"os"
	"path/filepath"
)

// Struct containing flag parameters for the `register` command
type RegisterFlags struct {
	Server string
}

// Parse command line flags for the `register` command
func ParseRegisterFlags(args []string) (*RegisterFlags, error) {
	registerCmd := flag.NewFlagSet("register", flag.ExitOnError)
	server := registerCmd.String("server", "", "URL of server to register on")

	registerCmd.Parse(args)
	if *server == "" {
		return nil, errors.New("server must be specified")
	}

	return &RegisterFlags{
		Server: *server,
	}, nil
}

// Struct containing flag parameters for the `post` command
type PostFlags struct {
	WorkingDir   string
	ConfigFile   string
	SourcesFile  string
	ImagesDir    string
	LastHashPath string
}

// Parses command line flags for the `post` command.
func ParsePostFlags(args []string) (*PostFlags, error) {
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	postCmd := flag.NewFlagSet("post", flag.ExitOnError)
	var flags PostFlags
	workingDir := postCmd.String("dir", cwd, "Directory of config and sources file")
	configFile := postCmd.String("config", "", "Path to config file (default: $dir/config.ini)")
	sourcesFile := postCmd.String("sources", "", "Path ro sources.txt file (default: $dir/sources.txt)")
	imagesDir := postCmd.String("images", "", "Path to folder containing local images (default: $dir/images)")
	lastHashPath := postCmd.String("last-hash-path", "", "Path to fole containing the hash of the last file used")
	postCmd.Parse(args)

	if *configFile == "" {
		flags.ConfigFile = filepath.Join(*workingDir, "config.ini")
	} else {
		flags.ConfigFile = *configFile
	}

	if *sourcesFile == "" {
		flags.SourcesFile = filepath.Join(*workingDir, "sources.txt")
	} else {
		flags.SourcesFile = *sourcesFile
	}

	if *imagesDir == "" {
		flags.ImagesDir = filepath.Join(*workingDir, "images")
	} else {
		flags.ImagesDir = *imagesDir
	}

	if *lastHashPath == "" {
		flags.LastHashPath = filepath.Join(*workingDir, "last-hash")
	} else {
		flags.LastHashPath = *lastHashPath
	}

	flags.WorkingDir = *workingDir
	return &flags, nil
}
