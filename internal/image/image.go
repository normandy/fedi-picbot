package image

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"codeberg.org/normandy/fedi-picbot/internal/flagparser"
)

type Sha256Hash []byte

type Image struct {
	Url       string
	File      *os.File
	path      string
	Source    string
	Sensitive bool
	Hash      Sha256Hash
}

var ErrDupe = errors.New("duplicate detected, hash of last and current file equal")

// Checks if a given hash matches the last file's hash.
// If it is, return an error.
func (img *Image) CheckDupe(lastHashPath string) error {
	f, err := os.Open(lastHashPath)

	if err != nil {
		return err
	}
	defer f.Close()

	lastHash := make([]byte, 32) // 32 bytes = 256 bits: length of a SHA256 hash
	if _, err := f.Read(lastHash); err != nil {
		return err
	}

	if bytes.Equal(img.Hash, lastHash) {
		return ErrDupe
	}
	return nil
}

// Saves the given hash of a file stream to last-hash.txt
func (img *Image) SaveHash(lastHashPath string) error {
	f, err := os.Create(lastHashPath)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err := f.Write(img.Hash); err != nil {
		return err
	}
	return nil
}

func (img *Image) Cleanup() error {
	err := img.File.Close()
	if err != nil {
		return err
	}

	if img.path != "" {
		err = os.Remove(img.path)
		if err != nil {
			return err
		}
	}
	return nil
}

// Get the SHA256 hash for a given Reader
func fileHash(file *os.File) (hash Sha256Hash, err error) {
	h := sha256.New()
	if _, err := io.Copy(h, file); err != nil {
		return nil, err
	}
	file.Seek(0, io.SeekStart)

	return h.Sum(nil), nil
}

// Downloads a file from a given URL and saves it to a temporary location
// returning a ReadCloser for that temporary file
func getImageFromHttp(url string) (file *os.File, path string, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, "", fmt.Errorf("unable to fetch image, received status %s", resp.Status)
	}

	f, err := os.CreateTemp("", "fedi-picbot-download")
	if err != nil {
		return nil, "", err
	}
	defer f.Close()

	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return nil, "", err
	}

	path = f.Name()
	file, err = os.Open(path)
	if err != nil {
		return nil, "", err
	}

	return file, path, err
}

// Select a random line from a file.
func randomLine(file *os.File) (pick string, pickNum int) {
	scanner := bufio.NewScanner(file)
	lineNum := 1
	for scanner.Scan() {
		line := scanner.Text()
		randomSrc := rand.NewSource(time.Now().UnixNano())
		random := rand.New(randomSrc)

		roll := random.Intn(lineNum)
		if roll == 0 {
			pick = line
			pickNum = lineNum
		}

		lineNum += 1
	}
	return pick, pickNum
}

func getImage(sourcesFilePath string, imagesDir string) (img *Image, err error) {
	sourcesFile, err := os.Open(sourcesFilePath)
	if err != nil {
		return nil, err
	}
	defer sourcesFile.Close()

	pick, pickNum := randomLine(sourcesFile)

	// Line format is <image url> <sensitive bool> <source>
	// each separated by tabs
	parts := strings.SplitN(pick, "\t", 3)
	if len(parts) != 3 {
		return nil, fmt.Errorf("line %d in sources.txt is not valid", pickNum)
	}

	url := parts[0]
	sensitive, err := strconv.ParseBool(parts[1])
	if err != nil {
		return nil, err
	}
	source := parts[2]

	var file *os.File
	var path string
	if strings.HasPrefix(url, "http://") || strings.HasPrefix(url, "https://") {
		file, path, err = getImageFromHttp(url)
		if err != nil {
			return nil, err
		}
	} else {
		url = filepath.Join(imagesDir, url)
		file, err = os.Open(url)
		if err != nil {
			return nil, err
		}
		path = ""
	}

	hash, err := fileHash(file)
	if err != nil {
		return nil, err
	}

	return &Image{
		Url:       url,
		File:      file,
		path:      path,
		Source:    source,
		Sensitive: sensitive,
		Hash:      hash,
	}, nil
}

// Tries to get an image from flags.SourcesFile, retrying up to maxRetires times.
func GetImageLoop(flags *flagparser.PostFlags, maxRetries int) (img *Image, err error) {
	imgSuccess := false

	for i := 1; i <= maxRetries && !imgSuccess; i++ {
		img, err = getImage(flags.SourcesFile, flags.ImagesDir)
		if err != nil {
			return nil, err
		}

		fmt.Printf("Posting image from %s\n", img.Url)

		err = img.CheckDupe(flags.LastHashPath)
		if errors.Is(err, ErrDupe) {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			if i+1 < maxRetries {
				fmt.Fprintf(os.Stderr, "Retry attempt #%d\n", i)
			}
			err = img.Cleanup()
			if err != nil {
				return nil, err
			}
			time.Sleep(5 * time.Second)
			continue
		} else if errors.Is(err, fs.ErrNotExist) {
			err = img.SaveHash(flags.LastHashPath)
			if err != nil {
				return nil, err
			}
		} else if err != nil {
			return nil, err
		}
		imgSuccess = true
	}

	if !imgSuccess {
		return nil, errors.New("max reties exceeded")
	}

	return img, nil
}
