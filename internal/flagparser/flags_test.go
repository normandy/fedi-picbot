package flagparser

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseRegisterFlags(t *testing.T) {
	expected := RegisterFlags{
		Server: "https://example.com/",
	}

	args := []string{"-server=https://example.com/"}
	actual, err := ParseRegisterFlags(args)
	if err != nil {
		t.Fatalf("Received error: %v", err)
	}

	if expected.Server != actual.Server {
		t.Errorf("Expected Server '%v', got '%v'", expected.Server, actual.Server)
	}
}

func TestParseRegisterFlagsNoServer(t *testing.T) {
	if _, err := ParseRegisterFlags(nil); err == nil {
		t.Fatal("Expected error, got none")
	}
}

func assertPostFlagsEqual(t *testing.T, expected *PostFlags, actual *PostFlags) {
	assert := assert.New(t)
	assert.Equal(expected.WorkingDir, actual.WorkingDir)
	assert.Equal(expected.ConfigFile, actual.ConfigFile)
	assert.Equal(expected.SourcesFile, actual.SourcesFile)
	assert.Equal(expected.ImagesDir, actual.ImagesDir)
	assert.Equal(expected.LastHashPath, actual.LastHashPath)
}

func TestParsePostFlagsDefaults(t *testing.T) {
	cwd, err := os.Getwd()
	if err != nil {
		t.Fatalf("Unable to get current working directory: %v", err)
	}

	expected := &PostFlags{
		WorkingDir:   cwd,
		ConfigFile:   filepath.Join(cwd, "config.ini"),
		SourcesFile:  filepath.Join(cwd, "sources.txt"),
		ImagesDir:    filepath.Join(cwd, "images"),
		LastHashPath: filepath.Join(cwd, "last-hash"),
	}

	actual, err := ParsePostFlags(nil)
	if err != nil {
		t.Fatalf("Received error: %v", err)
	}

	assertPostFlagsEqual(t, expected, actual)
}

func TestParsePostFlagsWorkdingDir(t *testing.T) {
	expected := &PostFlags{
		WorkingDir:   "blah",
		ConfigFile:   filepath.Join("blah", "config.ini"),
		SourcesFile:  filepath.Join("blah", "sources.txt"),
		ImagesDir:    filepath.Join("blah", "images"),
		LastHashPath: filepath.Join("blah", "last-hash"),
	}

	flags := []string{"-dir=blah"}
	actual, err := ParsePostFlags(flags)
	if err != nil {
		t.Fatalf("Received error: %v", err)
	}

	assertPostFlagsEqual(t, expected, actual)
}

func TestParsePostFlagsAllFlags(t *testing.T) {
	expected := &PostFlags{
		WorkingDir:   "blah",
		ConfigFile:   "config.ini",
		SourcesFile:  "sources.txt",
		ImagesDir:    "images",
		LastHashPath: "last-hash",
	}

	flags := []string{"-dir=blah", "-config=config.ini", "-sources=sources.txt", "-images=images", "-last-hash-path=last-hash"}
	actual, err := ParsePostFlags(flags)
	if err != nil {
		t.Fatalf("Received error: %v", err)
	}

	assertPostFlagsEqual(t, expected, actual)
}
