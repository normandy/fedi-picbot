# fedi-picbot

An image bot for Mastodon and Pleroma.

## Features
- Fetch images from URLs
- Provides a link back to the image source
- Can mark images as sensitive

## Installation
Requirements:
- Go 1.18+
- (optional) Make (tested with GNU Make)

`fedi-picbot` can be built in two ways:
1. Standard go: `go build`
2. Make: `make && make install`

If you're using Make, you can specify the install destination using the standard `PREFIX` and `DESTDIR` variables.

## Usage
Registering the bot on the instance:
```bash
./fedi-picbot register -server="https://instance.tld"
```

Making a post:
```bash
./fedi-picbot post [-dir=<path to dir with sources.txt and config.ini>]
```

**Note**: You can also specify the locations of `sources.txt`, `config.ini`, and the `images` directory separately using the `-sources`, `-config`, and `-images` flags respectively.

### sources.txt format
Each line in sources.txt is as follows:
```
<image-url> <sensitive-bool> <url-to-source> 
```

Notes:
- `<image-url>` can be an HTTP(S) url or a local filename
- Each component is separated by tabs
