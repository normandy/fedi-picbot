package config

import (
	"github.com/mattn/go-mastodon"
	"gopkg.in/ini.v1"
)

type LoginInfo struct {
	Username string
	Password string
}

type Config struct {
	MastodonConfig mastodon.Config
	Login          LoginInfo
}

// Loads a config from an INI source.
// configSource can be either a string containing the path to an INI file or
// the raw contents of an INI file as []byte.
func ConfigFromIni[T string | []byte](configSource T) (config *Config, err error) {
	ini, err := ini.Load(configSource)
	if err != nil {
		return nil, err
	}

	config = &Config{
		MastodonConfig: mastodon.Config{
			Server:       ini.Section("").Key("Server").String(),
			ClientID:     ini.Section("").Key("ClientID").String(),
			ClientSecret: ini.Section("").Key("ClientSecret").String(),
		},
		Login: LoginInfo{
			Username: ini.Section("Login").Key("Username").String(),
			Password: ini.Section("Login").Key("Password").String(),
		},
	}

	return config, nil
}
